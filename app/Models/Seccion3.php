<?php 
namespace App\Models;

use CodeIgniter\Model;

class Seccion3 extends Model{
    protected $table = 'seccioniii';
    protected $primaryKey = 'idSeccionIII';
    protected $allowedFields = ['RazonSocial', 'NombreComercial', 'Calle', 'Ciudad', 'Municipio', 'cPostal','Pais', 'PaginaWeb','fk_idAviso'];
}