<?php
namespace App\Models;

use CodeIgniter\Model;

class Sector extends Model{
    protected $table = 'sector';
    protected $primaryKey = 'idSector';
    protected $allowedFields = ['NombreSector'];
    
}