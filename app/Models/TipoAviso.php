<?php

namespace App\Models;

use CodeIgniter\Model;

class TipoAviso extends Model{
    protected $table = 'tipoaviso';
    protected $primaryKey = 'idTipoAviso';
    protected $allowerdFields = 'nombreTipoAviso, fk_idAviso';
}