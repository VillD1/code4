<?php
namespace App\Models;

use CodeIgniter\Model;

class Empresa extends Model{
    protected $table      = 'empresa';
    protected $primaryKey = 'idEmpresa';
    protected $allowedFields = ['fk_idAviso', 'fk_idRamas','fk_idSector'];
    protected $returnType = 'object';
}


?>