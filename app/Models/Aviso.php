<?php 
namespace App\Models;

use CodeIgniter\Model;

class Aviso extends Model{
    protected $table      = 'Aviso';
    protected $primaryKey = 'idAviso';
    protected $allowedFields = ['folioAviso', 'FechaCreacion','FechaModificacion', 'fk_idPersona', 'Estado','fk_idTipoEmpresa'];
    protected $returnType = 'object';
    
}