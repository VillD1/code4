<?php 
namespace App\Models;

use CodeIgniter\Model;

class Usuario extends Model{
    
    protected $table = 'persona';
    protected $primaryKey = 'idPersona';
    protected $allowedFields = ['RazonSocial', 'User', 'Pass', 'Email', 'Fecha', 'Estado' ];
    protected $returnType = 'object';

    public function getUserBy(string $column, string $value){
        return $this->where($column, $value)->first();
    }
    public function getLoggedInUserData($id)
    {
        $builder = $this->db->table('persona');
        $builder->where('idPersona',$id);
        $result = $builder->get();
        if(count($result->getResultArray())==1)
        {
            return $result->getRow();
        }
        else
        {
            return false;
        }
    }
    public function updateUserInfo($data,$id)
    {
        $bulder = $this->db->table('persona');
        $builder->where('idPersona',$id);
        $builder->update($data);
        if($this->db->affectedRows()>0)
        {
            return true;
        }else
        {
            return false;
        }
    }
}