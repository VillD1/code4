<?php
namespace App\Models;
use CodeIgniter\Model;

class TipoEmpresa extends Model{
    protected $table = 'tipoempresa';
    protected $primaryKey = 'idTipoEmpresa';
    protected $allowedFields = 'nombreTipo';
}