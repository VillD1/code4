<?php

namespace App\Models;

use CodeIgniter\Model;

class Ramas extends Model{

    protected $table = 'ramas';

    protected $primaryKey = 'idRamas';
    protected $allowedFields= ['NombreRamas', 'fk_idSector'];
    protected $returnType = 'array';
}