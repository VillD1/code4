<?php $this->extend("Template/Base");?>
<?= $this->section("content");?>
    <div class="contenido_seccion4 d-flex flex-column justify-content-center">
    <?= $this->include('Dashboard/header_user');?>
        <div class="mr-5 ml-5 border border-dark p-3">
            <form>
                <h4 class="mt-2">Seccion IV. Finalidades del tratamiento</h4>
                <h5 class="mt-3">1. Indique para que finalidades tratara los datos personales que recaba y especifique 
                si son o no necesarios para la relacion juridica que tiene con el celular </h5>
                <table class="table">
                    <thead>
                        <tr>
                            <th class="col-6 font-wight-normal">Finalidad</th>
                            <th class="col-6">¿Necesaria para la relación jurídica con el titular?</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td scope="row">1</td>
                            <td>Gustavo</td>
                        </tr>
                        <tr>
                            <td scope="row">2</td>
                            <td>Daniel</td>                    
                        </tr>
                        <tr>
                            <td scope="row">3</td>
                            <td>Manuel</td>                    
                        </tr>
                        <tr>
                            <td scope="row">4</td>
                            <td>Manuel</td>                    
                        </tr>
                    </tbody>
                </table>
                <h5 class="mt-3">2. Indique si tratará los datos personales para las siguientes finalidades en especifico:</h5>
                <table class="table">
                    <thead>
                        <tr>
                        <th scope="col-4 font-wight-normal">Finalidad</th>
                        <th scope="col-4">¿Se lleva a cabo el tratamiento?</th>
                        <th scope="col-4">¿Necesaria para la relación juridica con el titular?</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr >
                            <td scope="row">Mercadoctenia o publicitaria</td>
                            <td>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                                    <label class="form-check-label" for="exampleRadios2">Si
                                    </label>
                                
                                    <input class="form-check-input ml-2" type="radio" name="" id="exampleRadios2" value="option2">
                                    <label class="form-check-label ml-4" for="exampleRadios2">No
                                    </label>
                                </div>
                            </td>
                            <td>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                                    <label class="form-check-label" for="exampleRadios2">Si
                                    </label>
                                
                                    <input class="form-check-input ml-2" type="radio" name="" id="exampleRadios2" value="option2">
                                    <label class="form-check-label ml-4" for="exampleRadios2">No
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td scope="row">Prospección comercial</td>
                            <td>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                                    <label class="form-check-label" for="exampleRadios2">Si
                                    </label>
                                
                                    <input class="form-check-input ml-2" type="radio" name="" id="exampleRadios2" value="option2">
                                    <label class="form-check-label ml-4" for="exampleRadios2">No
                                    </label>
                                </div>
                            </td>
                            <td>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                                    <label class="form-check-label" for="exampleRadios2">Si
                                    </label>
                                
                                    <input class="form-check-input ml-2" type="radio" name="" id="exampleRadios2" value="option2">
                                    <label class="form-check-label ml-4" for="exampleRadios2">No
                                    </label>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table> 
            </form>

            <nav aria-label="paginacion" class="d-flex justify-content-center mt-3">
                <ul class="pagination pagination-md">
                    <li class="page-item ">
                        <a class="page-link" >1</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" >2</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#3">3</a>
                    </li>
                    <li class="page-item <?= service('request')->uri->getPath() == 'dash/seccion/seccion_cuatro'? 'active' :''?>">
                        <a href="<?= base_url(route_to('dash/seccion/seccion_cuatro'))?>" class="page-link" name="page#4">4</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#5">5</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#6">6</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#7">7</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#8">8</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#9">9</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#10">10</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#11">11</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#12">12</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#13">13</a>
                    </li>
                </ul>
            </nav>
            <div class="buttons_seccion1 d-flex justify-content-center mt-3 ">
                <a href="<?= base_url(route_to('dash/seccion/seccion_tres'))?>" class="btn btn-primary btn-sm mr-3 ml-3" name="regresar" id="btnRegresar">Regresar</a>
                <button type="button" class="btn btn-secondary btn-sm mr-3 ml-3"  name="limpiar" id="btnLimpiar">Limpiar</button>
                <a href="<?= base_url(route_to('dash/seccion/seccion_cinco'))?>" class="btn btn-primary btn-sm mr-3 ml-3"name="siguiente" id="btnSiguiente">Siguiente</a>
            </div>
        </div>
    </div>

<?= $this->endSection();?>
<?php $this->extend("Template/Base");?>