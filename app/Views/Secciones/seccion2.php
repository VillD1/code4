<?php $this->extend("Template/Base");?>
<?= $this->section("content");?>
    <div class="contenido_seccion2 d-flex flex-column justify-content-center">
            <?= $this->include('Dashboard/header_user');?>
        <div class="mr-5 ml-5 border border-dark p-3"">
            <form>
                <h4 class="mt-2">Seccion II. Modalidad del aviso de privacidad</h4>
                <h5 class="mt-3">1. Indique la modalidad de aviso de privacidad que desea generar en esta sesión</h5>
                
                <?php 
                    foreach($tipoAviso as $tipo)
                    {    
                        echo 

                        '<div class="form-check">
                        <input class="form-check-input" type="radio" name="tipoAviso" 
                        value="'.$tipo["idTipoAviso"].'">'.
                        '<label class="form-check-label font-weight-bold">Aviso de privacidad '.$tipo["nombreTipoAviso"].'</label>
                        </div>';
                    }
                ?>

                <div class="border border-dark mt-3 p-2">
                    <p >Nota aclaratoria:</p>
                    <p >Si usted va a generar un aviso de privacidad integral deberá responder todas las secciones con excepción de la XII, por lo que esta sección no aparecerá en el momento del llenado del cuestionario.</p>
                    <p >Si usted va a generar un aviso de privacidad corto o simplificado, además de las secciones I y II, sólo deberá responder las secciones III, IV y XII.</p>
                </div> 
            </form>

            <nav aria-label="paginacion" class="d-flex justify-content-center mt-3">
                <ul class="pagination pagination-md">
                    <li class="page-item ">
                        <a class="page-link" >1</a>
                    </li>
                    <li class="page-item <?= service('request')->uri->getPath() == 'dash/seccion/seccion_dos'? 'active' :''?> ">
                        <a href="<?= base_url(route_to('dash/seccion/seccion_dos'))?>" class="page-link" >2</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#3">3</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#4">4</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#5">5</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#6">6</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#7">7</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#8">8</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#9">9</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#10">10</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#11">11</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#12">12</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#13">13</a>
                    </li>
                </ul>
            </nav>
            <div class="buttons_seccion1 d-flex justify-content-center mt-3 ">
                <a class="btn btn-primary btn-sm mr-3 ml-3" name="regresar" href="<?= base_url(route_to('dash/seccion/seccion_uno'))?>" >Regresar</a>
                <a class="btn btn-secondary btn-sm mr-3 ml-3"  name="limpiar" id="btnLimpiar">Limpiar</a>
                <a class="btn btn-primary btn-sm mr-3 ml-3"name="siguiente" href="<?= base_url(route_to('dash/seccion/seccion_tres'))?>">Siguiente</a>
            </div>
        </div>
    </div>

<?= $this->endSection();?>
<?php $this->extend("Template/Base");?>