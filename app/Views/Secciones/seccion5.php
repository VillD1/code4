<?php $this->extend("Template/Base");?>
<?= $this->section("content");?>
    <div class="contenido_seccion5 d-flex flex-column justify-content-center">
    <?= $this->include('Dashboard/header_user');?>
        <div class="mr-5 ml-5 mb-5 border border-dark p-3">
            <form>
                <h4 class="mt-2">Seccion V. Datos personales recabados</h4>
                <h5 class="mt-3">1. ¿Cómo desea informar sobre los datos personales que tratará</h5>

                <div class="form-check">
                    <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                    <label class="form-check-label" for="exampleRadios2">Si</label>
                    <br/>    
                    <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                    <label class="form-check-label" for="exampleRadios2">No</label>
                </div>
                <h5 class="mt-3 ">2. Indique los datos personales que tratará para las finalidades informadas en el aviso de privacidad:</h5>
                <p class="h5 mt-4">Datos de identificacion y contacto</p>
                <div class="form-check">
                    <input class="form-check-input " type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Nombre</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Estado civil</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">RUC</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Lugar nacimiento</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Fecha nacimiento</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Nacionalidad</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Domicilio</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Teléfono particular</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Teléfono celular</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Correo eléctronico</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Firma y autógrafia</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Firma eléctronica</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Edad</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Fotografía</label><br>
                </div>
                
                <p class="h5 mt-4">Datos de identificacion y contacto</p>
                <div class="form-check">
                    <input class="form-check-input " type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Color de la piel</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Color del iris</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Color de cabello</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Señales particulares</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Estatura</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Peso</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Cicatrices</label><br>
                </div>

                <p class="h5 mt-4">Datos biométricos</p>
                <div class="form-check">
                    <input class="form-check-input " type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Imagen de iris</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Huella dactilar</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Palma de la mano</label><br>
                </div>

                <p class="h5 mt-4">Datos laborales</p>
                <div class="form-check">
                    <input class="form-check-input " type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Puesto o cargo que desempeña</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Domicilio de trabajo</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Correo electronico institucional</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Telefono institucional</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Referencias laborales</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Capacitación laboral</label><br>
                </div>

                <p class="h5 mt-4">Datos académicos</p>
                <div class="form-check">
                    <input class="form-check-input " type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Trayectoria educativa</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Títulos</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Cédula profesional</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Certificados</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Reconocimientos</label><br>
                </div>

                <p class="h5 mt-4">Datos migratorios</p>
                <div class="form-check">
                    <input class="form-check-input " type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Entradas al país</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Salidas del país</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Tiempo de permanencia en el país</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Calidad de tiempo migratoria</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Aseguramiento</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Derechos de reparticion</label><br>
                </div>

                <p class="h5 mt-4">Datos primitivos y/o financieron</p>
                <div class="form-check">
                    <input class="form-check-input " type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Bienes muebles</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Bienes inmuebles</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Información físcal</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Historial de credito</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Egresos</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Ingresos </label><br>
                    
                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Egresos</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Ingresos </label><br>
                </div>

                <p class="h5 mt-4">Datos sobre pasatiempos, entretenimiento y diveresión</p>
                <div class="form-check">
                    <input class="form-check-input " type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Pasatiempos</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Aficiones</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Deportes que práctica</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Juegos de su interés</label><br>
                </div>
                <h5 class="mt-3 ">3. Indique las categorías de datos personales que tratará las finalidades en el aviso de privacidad: </h5>
                <div class="form-check">
                    <input class="form-check-input " type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Datos de indentificación</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Datos de de contacto</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Datos sobre caracteristicas fisicas</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Datos biométricos</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Datos laborales</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Datos académicos</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Datos migratorios</label><br>
                    
                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Datos patrimoniales y financieros</label><br>
                </div>
                <h5 class="mt-3 ">4. Indique si tratará datos personales sensibles </h5>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                    <label class="form-check-label" for="exampleRadios2">Si</label>
                    <br/>    
                    <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                    <label class="form-check-label" for="exampleRadios2">No</label>
                </div>
                <h5 class="mt-3 ">5. ¿Cómo desea informar sobre los datos personales sensibles que tratará?</h5>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                    <label class="form-check-label" for="exampleRadios2">A trávez un listado que por su nombre identifique cada uno de los datos personales sensibles.</label>
                    <br/>    
                    <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                    <label class="form-check-label" for="exampleRadios2">A trávez de un catálogo que identifique las categorías de los datos personales sensibles.</label>
                </div>
                <h5 class="mt-3 ">6. Indique los datos personales sensibles que tratará para las finalidades informadas en el aviso de privacidad:</h5>
                <p class="h5 mt-4">Datos sobre la ideología: creencias religiosas, filosóficas o morales, opiniones políticas y afiliación sindical:</p>
                <div class="form-check">
                    <input class="form-check-input " type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Posturas ideológicas</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Religión que profesa</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Posturas filosóficas</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Posturas morales</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Posturas políticas</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Pertenencia a un sindicato</label><br>
                </div>

                <p class="h5 mt-4">Datos de salud</p>
                <div class="form-check">
                    <input class="form-check-input " type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Estado de salud físico presente, pasado o futuro</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Estado de salud mental presente, pasado o futuro</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Información genética</label><br>
                </div>

                <p class="h5 mt-4">Datos sobre vida sexual</p>
                <div class="form-check">
                    <input class="form-check-input " type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Preferencias sexuales</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Prácticas o habitos sexuales</label><br>
                </div>

                <p class="h5 mt-4">Datos de origen étnico o racial</p>
                <div class="form-check">
                    <input class="form-check-input " type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Pertenencia a un pueblo, etnia o región</label><br>
                </div>
                <h5 class="mt-3 ">7. Indique las categorías de datos personales sensibles que tratará para las finalidades informadas en el aviso de privacidad:</h5>
                <div class="form-check">
                    <input class="form-check-input " type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Datos sobre ideología: creencias religiosas, filosóficas o morales</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Datos de salud</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Datos sobre vida sexual</label><br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Datos de origen étnico o racial</label><br>
                </div>
                
            </form>

            <nav aria-label="paginacion" class="d-flex justify-content-center mt-3">
                <ul class="pagination pagination-md">
                    <li class="page-item ">
                        <a class="page-link" >1</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" >2</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#3">3</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#4">4</a>
                    </li>
                    <li class="page-item <?= service('request')->uri->getPath() == 'dash/seccion/seccion_cinco'? 'active' :''?>">
                        <a href="<?= base_url(route_to('dash/seccion/seccion_cinco'))?>" class="page-link" name="page#5">5</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#6">6</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#7">7</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#8">8</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#9">9</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#10">10</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#11">11</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#12">12</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#13">13</a>
                    </li>
                </ul>
            </nav>
            <div class="buttons_seccion1 d-flex justify-content-center mt-3 ">
                <a href="<?= base_url(route_to('dash/seccion/seccion_cuatro'))?>" class="btn btn-primary btn-sm mr-3 ml-3" name="regresar" id="btnRegresar">Regresar</a>
                <button type="button" class="btn btn-secondary btn-sm mr-3 ml-3"  name="limpiar" id="btnLimpiar">Limpiar</button>
                <a href="<?= base_url(route_to('dash/seccion/seccion_seis'))?>" class="btn btn-primary btn-sm mr-3 ml-3"name="siguiente" id="btnSiguiente">Siguiente</a>
            </div>
        </div>
    </div>
    
    <?= $this->endSection();?>
    <?php $this->extend("Template/Base");?>