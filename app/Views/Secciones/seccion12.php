<?php $this->extend("Template/Base");?>
<?= $this->section("content");?>
    <div class="contenido_seccion12 d-flex flex-column justify-content-center">
    <?= $this->include('Dashboard/header_user');?>
        <div class="Formulario mr-5 ml-5 border border-dark p-3">
            <form class="">
                <h4 class="mt-2">Seccion XII. Mecanismos para que el titular conozca el texto completo del aviso de privacidad</h4>
                <h5 class="mt-3">1. Señale el o los mecanismos para que el titular pueda conocer el aviso de privacidad integral:</h5>
                <input type="text"  class="form-control">
            </form>

            <nav aria-label="paginacion" class="d-flex justify-content-center mt-3">
                <ul class="pagination pagination-md">
                <li class="page-item ">
                        <a class="page-link" >1</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" >2</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#3">3</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#4">4</a>
                    </li>
                    <li class="page-item ">
                        <a class="page-link" name="page#5">5</a>
                    </li>
                    <li class="page-item" >
                        <a class="page-link" name="page#6">6</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#7">7</a>
                    </li>
                    <li class="page-item ">
                        <a class="page-link" name="page#8">8</a>
                    </li>
                    <li class="page-item ">
                        <a class="page-link" name="page#9">9</a>
                    </li>
                    <li class="page-item ">
                        <a  class="page-link" name="page#10">10</a>
                    </li>
                    <li class="page-item ">
                        <a class="page-link" name="page#11">11</a>
                    </li>
                    <li class="page-item <?= service('request')->uri->getPath() == 'dash/seccion/seccion_doce'? 'active' :''?>">
                        <a href="<?= base_url(route_to('dash/seccion/seccion_doce'))?>" class="page-link" name="page#12">12</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#13">13</a>
                    </li>
                </ul>
            </nav>
            <div class="buttons_seccion1 d-flex justify-content-center mt-3 ">
                <a href="<?= base_url(route_to('dash/seccion/seccion_once'))?>" class="btn btn-primary btn-sm mr-3 ml-3" name="regresar" id="btnRegresar">Regresar</a>
                <button type="button" class="btn btn-secondary btn-sm mr-3 ml-3"  name="limpiar" id="btnLimpiar">Limpiar</button>
                <a href="<?= base_url(route_to('dash/seccion/seccion_trece'))?>" class="btn btn-primary btn-sm mr-3 ml-3"name="siguiente" id="btnSiguiente">Siguiente</a>
            </div>
        </div>

    </div>
<?= $this->endSection();?>
<?php $this->extend("Template/Base");?>