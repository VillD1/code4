<?php $this->extend("Template/Base");?>
<?= $this->section("content");?>
    <div class="contenido_seccion1 d-flex flex-column justify-content-center" id="contenido_seccion">

        <?= $this->include('Dashboard/header_user');?>
        <?php
            
            require_once ('app\Controllers\Avisos\Crud_Avisos.php');
            
            $avisoModel = new \App\Controllers\Avisos\Crud_Avisos();
            
            
            if(isset($_POST['action']) == 'crear_aviso'){
                echo 'accion';
            }
            /*$idAviso = $_GET['id'];
            if(isset($_POST['tipoEmpresa'])){
                $tipoEmpresa = $_POST['tipoEmpresa'];
                $updateTipoEmpresa = $avisoModel->updateAviso($tipoEmpresa, $idAviso);
            }*/
            
            

            /*if(isset($_GET['siguiente'])){
                $id = $avisoModel->saveAviso();
                echo "<script> window.location.href = '/dash/seccion/seccion_dos?id=".$id."';</script>";
            }*/
        ?>
        <div class="formulario mr-5 ml-5 border border-dark p-3">
            <form action="crear_aviso" method="post">
                <h4 class="mt-2">Seccion I. Información estadística:</h4>
                <h5 class="mt-3">1. Indique la actividad principal del responsable:</h5>

                <h5 class="mt-3 mb-3">Sectores:</h5>
                <select name="sector" class="custom-select custom-select-sm" id="sector">
                    <option disabled selected>ELIJE UN SECTOR</option>
                    <?php 
                    foreach($sectores as $sector)
                    {    
                        echo '<option value="'.$sector["idSector"].'">'.$sector["NombreSector"].'</option>';
                    }
                    ?>
                </select>

                <h5 class="mt-3 mb-3">Ramas</h5>
                <select class="custom-select custom-select-sm" id="ramas" name="ramas">
                    
                </select>
                <h4 class="mt-2">2. Indique si se trata de una micro, pequeña, mediana o gran empresa:</h4>

                <?php 
                    foreach($tipoEmpresa as $tipo)
                    {    
                        echo 
                        '<div class="form-check">
                        <input class="form-check-input" type="radio" name="tipoEmpresa" value="'.$tipo["idTipoEmpresa"].'">'.$tipo["nombreTipo"].'
                        </div>';
                    }
                ?>

            <nav aria-label="paginacion" class="d-flex justify-content-center mt-3">
                <ul class="pagination pagination-md">
                    
                    <li class="page-item <?= service('request')->uri->getPath() == 'dash/seccion/seccion_uno'? 'active' :''?>">
                        <a href="<?= base_url(route_to('dash/seccion/seccion_uno'))?>" class="page-link" >1</a>
                    </li>
                    <li class="page-item ">
                        <a class="page-link">2</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#3">3</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#4">4</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#5">5</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#6">6</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#7">7</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#8">8</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#9">9</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#10">10</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#11">11</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#12">12</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#13">13</a>
                    </li>
                </ul>
            </nav>
            <div class="buttons_seccion1 d-flex justify-content-center mt-3 ">
                <button type="submit" class="btn btn-primary btn-sm mr-3 ml-3 disabled" name="regresar">Regresar</button>
                <button type="submit" class="btn btn-secondary btn-sm mr-3 ml-3" name="limpiar" id="btnLimpiar">Limpiar</button>
                    
                <button type="submit" class="btn btn-primary btn-sm mr-3 ml-3" name="siguiente">Siguiente</button>
                </form>
            </div>
        </div> 
    </div> 
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>

    <script>
        $(document).ready(function(){
            $('#sector').change(function(){
                var idSector =  $('#sector').val();
                var action = 'get_ramas';

                if(idSector != '')
                {
                    $.ajax({
                        url:"<?php echo base_url('/Avisos/Avisos/action'); ?>",
                        method:"POST",
                        data:{idSector:idSector, action:action},
                        dataType:"JSON",
                        success:function(data)
                        {
                            var html = '<option value="">ELIJE UNA RAMA</option>';
                            for(var count=0; count<data.length; count++)
                            {
                                html +='<option value="'+ data[count].idRamas+'">'+data[count].NombreRamas+'</option>';
                            }
                            $('#ramas').html(html);
                        }
                    });
                }
                else
                {
                    $('#ramas').val('');
                }
                
            });
        });
    </script>
    
    
<?= $this->endSection();?>
<?php $this->extend("Template/Base");?>