<?php $this->extend("Template/Base");?>
<?= $this->section("content");?>
    <div class="contenido_seccion6 d-flex flex-column justify-content-center">
    <?= $this->include('Dashboard/header_user');?>
        <div class="mr-5 ml-5 mb-5 border border-dark p-3">
            <form>
                <h4 class="mt-2">Seccion VII. Medidas y procedimiento para el ejercicio de los derechos de acceso, rectificación. cancelacón y oposición(ARCO)</h4>
                <h5 class="mt-3">1. Describa los medios que el titular tiene a su disposición para ejercer derechos ARCO:</h5>
                <input type="text"  class="form-control">
                <h5 class="mt-3">2. ¿Cómo desea informar sobre el procedimiento que ha implementado para atender las solicitudes de derechos ARCO</h5>

                <div class="form-check">
                    <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                    <label class="form-check-label" for="exampleRadios2">Describiendo el procedimiento en el cuerpo del aviso de privacidad</label>
                    <br/>    
                    <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                    <label class="form-check-label" for="exampleRadios2">Proporcionando el medio por el cual el titular puede conocer este procedimiento</label>
                </div>

                <h5 class="mt-3">3. Responde las siguientes preguntas sobre el procedimiento:</h5>

                <div class="form-group">
                    <label class="form-check-label" for="exampleRadios2">¿A través de que medios pueden acreditar su identidad el titular y, en su caso, su representante, así como la personalidad este último?</label>
                    <input type="text" class="form-control mt-3" id="formGroupExampleInput">
                    <br/>

                    <label class="form-check-label" for="exampleRadios2">¿Qué información y/o documentación deberá contener la solicitud</label>
                    <input type="text" class="form-control mt-3" id="formGroupExampleInput">
                    <br/>

                    <label class="form-check-label" for="exampleRadios2">¿En cuántos días le daremos respuesta a su solicitud?</label>
                    <input type="text" class="form-control mt-3" id="formGroupExampleInput">
                    <br/>

                    <label class="form-check-label" for="exampleRadios2">¿Por qué medio le comunicaremos la respuesta a su solicitud?</label>
                    <input type="text" class="form-control mt-3" id="formGroupExampleInput">
                    <br/>
                    
                    <label class="form-check-label" for="exampleRadios2">¿En qué medios se pueden reproducir los datos personales que, en su caso, solicite?</label>
                    <input type="text" class="form-control mt-3" id="formGroupExampleInput">
                    <br/>

                    <label class="form-check-label" for="exampleRadios2">¿Ponemos a sus órdenes los siguientes formularios o sistemas para facilitar el ejercicio de derechos ARCO?
                    </label>
                    <input type="text" class="form-control mt-3" id="formGroupExampleInput">
                    <br/>

                    <label class="form-check-label" for="exampleRadios2">Para mayor información sobre el procedimiento, ponemos a disposición los siguientes medios</label>
                    <input type="text" class="form-control mt-3" id="formGroupExampleInput">
                    <br/>
                </div>

                <h5 class="mt-3">4. Describa los medios a través de los cuales el titular podrá obtener información sobre el procedimiento: </h5>
                <input type="text" class="form-control mt-3" id="formGroupExampleInput">

                <h5 class="mt-3">5. Proporcione los siguientes datos de contacto de la persona o departamento de datos personales </h5>
                    
                <div class="form-group">
                    <label class="form-check-label" for="exampleRadios2">Nombre de la persona o departamento de datos personales:</label>
                    <input type="text" class="form-control mt-3" id="formGroupExampleInput">
                    <br/>

                    <label class="form-check-label" for="exampleRadios2">Domicilio</label>
                    <input type="text" class="form-control mt-3" id="formGroupExampleInput" placeholder="Calle y número">
                    <br/>

                    
                    <input type="text" class="form-control mt-3" id="formGroupExampleInput" placeholder="Calle y número">
                    <br/>
                    
                    
                    <input type="text" class="form-control mt-3" id="formGroupExampleInput" placeholder="Calle y número">
                    <br/>

                    
                    <input type="text" class="form-control mt-3" id="formGroupExampleInput" placeholder="Calle y número">
                    <br/>

                    
                    <input type="text" class="form-control mt-3" id="formGroupExampleInput" placeholder="Calle y número">
                    <br/>

                    <label class="form-check-label" for="exampleRadios2">Correo electrónico</label>
                    <input type="text" class="form-control mt-3" id="formGroupExampleInput">
                    <br/>

                    <label class="form-check-label" for="exampleRadios2">Número telefónico</label>
                    <input type="text" class="form-control mt-3" id="formGroupExampleInput">

                    <br/>

                    <label class="form-check-label" for="exampleRadios2">Otro dato de contacto</label>
                    <input type="text" class="form-control mt-3" id="formGroupExampleInput">
                </div> 
            </form>

            <nav aria-label="paginacion" class="d-flex justify-content-center mt-3">
                <ul class="pagination pagination-md">
                    <li class="page-item ">
                        <a class="page-link" >1</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" >2</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#3">3</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#4">4</a>
                    </li>
                    <li class="page-item ">
                        <a class="page-link" name="page#5">5</a>
                    </li>
                    <li class="page-item" >
                        <a class="page-link" name="page#6">6</a>
                    </li>
                    <li class="page-item <?= service('request')->uri->getPath() == 'dash/seccion/seccion_siete'? 'active' :''?>">
                        <a href="<?= base_url(route_to('dash/seccion/seccion_siete'))?>" class="page-link" name="page#7">7</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#8">8</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#9">9</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#10">10</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#11">11</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#12">12</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#13">13</a>
                    </li>
                </ul>
            </nav>
            <div class="buttons_seccion1 d-flex justify-content-center mt-3 ">
                <a href="<?= base_url(route_to('dash/seccion/seccion_seis'))?>" class="btn btn-primary btn-sm mr-3 ml-3" name="regresar" id="btnRegresar">Regresar</a>
                <button type="button" class="btn btn-secondary btn-sm mr-3 ml-3"  name="limpiar" id="btnLimpiar">Limpiar</button>
                <a href="<?= base_url(route_to('dash/seccion/seccion_ocho'))?>" class="btn btn-primary btn-sm mr-3 ml-3"name="siguiente" id="btnSiguiente">Siguiente</a>
            </div>
        </div>
    </div>
<?= $this->endSection();?>
<?php $this->extend("Template/Base");?>