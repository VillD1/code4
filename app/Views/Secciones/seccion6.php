<?php $this->extend("Template/Base");?>
<?= $this->section("content");?>
    <div class="contenido_seccion6 d-flex flex-column justify-content-center">
    <?= $this->include('Dashboard/header_user');?>
        <div class="mr-5 ml-5 mb-5 border border-dark p-3">
            <form>
                <h4 class="mt-2">Seccion VI. Transferencia de datos personales</h4>
                <h5 class="mt-3">1. ¿Lleva a cabo transferencias de datos personales, es decir, comparte los datos personales con terceros?</h5>

                <div class="form-check">
                    <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                    <label class="form-check-label" for="exampleRadios2">Si</label>
                    <br/>    
                    <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                    <label class="form-check-label" for="exampleRadios2">No</label>
                </div>
                <h5 class="mt-3">2. ¿Las transferencias de datos personales se hacen dentro o fuera del país</h5>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                    <label class="form-check-label" for="exampleRadios2">Dentro del país</label>
                    <br/>    
                    <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                    <label class="form-check-label" for="exampleRadios2">Fuera del país</label>
                    <br/>    
                    <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                    <label class="form-check-label" for="exampleRadios2">Ambos</label>
                </div>
                <h5 class="mt-3">3. ¿Quiénes son los destinatarios o terceros receptores de las transferencias que lleva a cabo y para qué finalidades transfiere los datos personales?¿Requiere consentimiento para los transferencias?</h5>
                <table class="table">
                    <thead>
                        <tr>
                        <th style="width:40%"scope="col-4">Destinario de los datos personales</th>
                        <th scope="col-4">Finalidad</th>
                        <th scope="col-4">Require del consentimiento</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr >
                            <td scope="row"</td>
                            <td></td>
                            <td>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                                    <label class="form-check-label" for="exampleRadios2">Si
                                    </label>
                                
                                    <input class="form-check-input ml-2" type="radio" name="" id="exampleRadios2" value="option2">
                                    <label class="form-check-label ml-4" for="exampleRadios2">No
                                    </label>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td scope="row"></td>
                            <td></td>
                            <td>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                                    <label class="form-check-label" for="exampleRadios2">Si
                                    </label>
                                
                                    <input class="form-check-input ml-2" type="radio" name="" id="exampleRadios2" value="option2">
                                    <label class="form-check-label ml-4" for="exampleRadios2">No
                                    </label>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <h5 class="mt-3">4. ¿Algunas de las transferencias antes indicadas requieren del consentimiento del titular?</h5>

                <div class="form-check">
                    <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                    <label class="form-check-label" for="exampleRadios2">Si</label>
                    <br/>    
                    <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                    <label class="form-check-label" for="exampleRadios2">No</label>
                </div>


                <h5 class="mt-3">5. ¿Qué tipo de consentimiento requiere?</h5>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                    <label class="form-check-label" for="exampleRadios2">Tácito</label>
                    <br/>    
                    <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                    <label class="form-check-label" for="exampleRadios2">Expreso</label>
                    <br/>    
                    <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                    <label class="form-check-label" for="exampleRadios2">Expreso y escrito</label>
                </div>

                <h5 class="mt-3">6. ¿Deseas solicitar el consentimiento para la transferencia a tráves del aviso de privacidad</h5>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                    <label class="form-check-label" for="exampleRadios2">Si</label>
                    <br/>    
                    <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                    <label class="form-check-label" for="exampleRadios2">No</label>
                </div> 
            </form>

            <nav aria-label="paginacion" class="d-flex justify-content-center mt-3">
                <ul class="pagination pagination-md">
                    <li class="page-item ">
                        <a class="page-link" >1</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" >2</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#3">3</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#4">4</a>
                    </li>
                    <li class="page-item ">
                        <a class="page-link" name="page#5">5</a>
                    </li>
                    <li class="page-item <?= service('request')->uri->getPath() == 'dash/seccion/seccion_seis'? 'active' :''?>" >
                        <a href="<?= base_url(route_to('dash/seccion/seccion_seis'))?>" class="page-link" name="page#6">6</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#7">7</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#8">8</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#9">9</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#10">10</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#11">11</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#12">12</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#13">13</a>
                    </li>
                </ul>
            </nav>
            <div class="buttons_seccion1 d-flex justify-content-center mt-3 ">
                <a href="<?= base_url(route_to('dash/seccion/seccion_cinco'))?>" class="btn btn-primary btn-sm mr-3 ml-3" name="regresar" id="btnRegresar">Regresar</a>
                <button type="button" class="btn btn-secondary btn-sm mr-3 ml-3"  name="limpiar" id="btnLimpiar">Limpiar</button>
                <a href="<?= base_url(route_to('dash/seccion/seccion_siete'))?>" class="btn btn-primary btn-sm mr-3 ml-3"name="siguiente" id="btnSiguiente">Siguiente</a>
            </div>
        </div>
    </div>

<?= $this->endSection();?>
<?php $this->extend("Template/Base");?>