<?php $this->extend("Template/Base");?>
<?= $this->section("content");?>
    <div class="contenido_seccion9 d-flex flex-column justify-content-center">
    <?= $this->include('Dashboard/header_user');?>
        <div class="mr-5 ml-5 border border-dark p-3">
            <form>
                <h4 class="mt-2">Seccion IX. Opciones y medios para limitar el uso y divulgación de los datos personales</h4>
                <h5 class="mt-3">1. Describa las opciones y medios para que el titular pueda limitar el uso o divulgación de sus datos personales:</h5>
                <input type="text"  class="form-control">
                <h5 class="mt-3">2. Indique si alguno de estos registros le aplica:</h5>
                <div class="form-check">
                    <input class="form-check-input " type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Registros públicos para evitar publicidad</label>
                    <br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Registros públicos de usuarios</label>
                </div>

                <h5 class="mt-3">3. ¿Cuenta con listados de exclusión en los que el titular pueda inscribirse para que sus datos personales no sean tratados o dejen de ser tratados para ciertas finalidades</h5>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                    <label class="form-check-label" for="exampleRadios2">Si</label>
                    <br/>    
                    <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                    <label class="form-check-label" for="exampleRadios2">No</label>
                </div>

                <h5 class="mt-3">4. Proporcione la siguiente información respecto de los listados de exclusión:</h5>

                <table class="table">
                    <thead>
                        <tr>
                        <th scope="col-4 font-wight-normal">Nombre de los listados</th>
                        <th scope="col-4">Finalidades para las que se aplica</th>
                        <th style="width:40%"scope="col-4">Medios a través del cual se puede obtener mayor información acerca del listado (número telefónico, correo eléctronico, entre otros).</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr >
                            <td scope="row">Mercadoctenia o publicitaria</td>
                            <td>1</td>
                            <td>2</td>
                        </tr>
                    </tbody>
                </table>
            </form>

            <nav aria-label="paginacion" class="d-flex justify-content-center mt-3">
                <ul class="pagination pagination-md">
                    <li class="page-item ">
                        <a class="page-link" >1</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" >2</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#3">3</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#4">4</a>
                    </li>
                    <li class="page-item ">
                        <a class="page-link" name="page#5">5</a>
                    </li>
                    <li class="page-item" >
                        <a class="page-link" name="page#6">6</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#7">7</a>
                    </li>
                    <li class="page-item ">
                        <a class="page-link" name="page#8">8</a>
                    </li>
                    <li class="page-item <?= service('request')->uri->getPath() == 'dash/seccion/seccion_nueve'? 'active' :''?>">
                        <a href="<?= base_url(route_to('dash/seccion/seccion_nueve'))?>" class="page-link" name="page#9">9</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#10">10</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#11">11</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#12">12</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#13">13</a>
                    </li>
                </ul>
            </nav>
            <div class="buttons_seccion1 d-flex justify-content-center mt-3 ">
                <a href="<?= base_url(route_to('dash/seccion/seccion_ocho'))?>" class="btn btn-primary btn-sm mr-3 ml-3" name="regresar" id="btnRegresar">Regresar</a>
                <button type="button" class="btn btn-secondary btn-sm mr-3 ml-3"  name="limpiar" id="btnLimpiar">Limpiar</button>
                <a href="<?= base_url(route_to('dash/seccion/seccion_diez'))?>" class="btn btn-primary btn-sm mr-3 ml-3" name="siguiente" id="btnSiguiente">Siguiente</a>
            </div>
        </div>
    </div>

<?= $this->endSection();?>
<?php $this->extend("Template/Base");?>