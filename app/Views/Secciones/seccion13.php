<?php $this->extend("Template/Base");?>
<?= $this->section("content");?>
    <div class="contenido_seccion13 d-flex flex-column justify-content-center">
    <?= $this->include('Dashboard/header_user');?>
        <form class="mr-5 ml-5 border border-dark p-3">
            <h4 class="mt-2">Seccion XIII. consentimiento del titular</h4>
            <h5 class="mt-3">1. ¿Requiere del consentimiento del titular para todos o algunos de los tratamientos informados en el aviso de privcacidad?</h5>
            <div class="form-check">    
                <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                <label class="form-check-label" for="exampleRadios2">Si</label>
                <br/>    
                <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                <label class="form-check-label" for="exampleRadios2">No</label>
            </div>
            <h5 class="mt-3">2. En caso de que requiera el consentimiento expreso y por escrito del titular para todos o algunos de los tratamientos informados en el aviso de privacidad ¿Desea obtenerlo a través del aviso de privacidad?</h5>
            <div class="form-check">    
                <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                <label class="form-check-label" for="exampleRadios2">Si</label>
                <br/>    
                <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                <label class="form-check-label" for="exampleRadios2">No</label>
            </div>
            <h5 class="mt-3">3. ¿Qué tipo de consentimiento requiere?</h5>
            <div class="form-check">    
                <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                <label class="form-check-label" for="exampleRadios2">Expreso</label>
                <br/>    
                <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                <label class="form-check-label" for="exampleRadios2">Expreso y por escrito</label>
            </div>

            <nav aria-label="paginacion" class="d-flex justify-content-center mt-3">
                <ul class="pagination pagination-md">
                    <li class="page-item ">
                        <a class="page-link" >1</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" >2</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#3">3</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#4">4</a>
                    </li>
                    <li class="page-item ">
                        <a class="page-link" name="page#5">5</a>
                    </li>
                    <li class="page-item" >
                        <a class="page-link" name="page#6">6</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#7">7</a>
                    </li>
                    <li class="page-item ">
                        <a class="page-link" name="page#8">8</a>
                    </li>
                    <li class="page-item ">
                        <a class="page-link" name="page#9">9</a>
                    </li>
                    <li class="page-item ">
                        <a  class="page-link" name="page#10">10</a>
                    </li>
                    <li class="page-item ">
                        <a class="page-link" name="page#11">11</a>
                    </li>
                    <li class="page-item ">
                        <a  class="page-link" name="page#12">12</a>
                    </li>
                    <li class="page-item <?= service('request')->uri->getPath() == 'dash/seccion/seccion_trece'? 'active' :''?>">
                        <a href="<?= base_url(route_to('dash/seccion/seccion_trece'))?>" class="page-link" name="page#13">13</a>
                    </li>
                </ul>
            </nav>
            <div class="buttons_seccion1 d-flex justify-content-center mt-3 ">
                <a href="<?= base_url(route_to('dash/seccion/seccion_doce'))?>" class="btn btn-primary btn-sm mr-3 ml-3" name="regresar" id="btnRegresar">Regresar</a>
                <button type="button" class="btn btn-secondary btn-sm mr-3 ml-3"  name="limpiar" id="btnLimpiar">Limpiar</button>
                <a class="btn btn-primary btn-sm mr-3 ml-3 disabled"name="siguiente" id="btnSiguiente">Siguiente</a>
            </div>
        </form>
    </div>
<?= $this->endSection();?>
<?php $this->extend("Template/Base");?>