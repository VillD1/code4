<?php $this->extend("Template/Base");?>
<?= $this->section("content");?>
    <div class="contenido_seccion3 d-flex flex-column justify-content-center">
    <?= $this->include('Dashboard/header_user');?>
        <div class="formulario mr-5 ml-5 mb-5 border border-dark p-3">

            <form method="POST" action="<?= base_url('Avisos/Avisos/seccion_3_save')?>">

                <h4 class="mt-2">Seccion III. Identidad y domicilio del responsable</h4>
                <h5 class="mt-3">1. Indique el nombre completo (nombre y apellidos), si el responsable se trata de una persona física, o bien, la denominación o razón social, si es una persona moral (de acuerdo con el acta constitutiva): </h5>
                <input type="text" class="form-control" name="campo_1_page_3">
                <h5 class="mt-2">2. Indique el nombre del responsable:</h5>
                <input type="text" class="form-control" name="campo_2_page_3">
                <h5 class="mt-2">3. Indique el nombre del responsable:</h5>
                <div class="form-group">
                    <input type="text" class="form-control mt-3" placeholder="Calle y número" name="campo_3_page_3">
                    
                    <input type="text" class="form-control mt-3" placeholder="Ciudad" name="campo_4_page_3">
                    <input type="text" class="form-control mt-3" placeholder="Municipio o delegación" name="campo_5_page_3">
                    <input type="text" class="form-control mt-3" placeholder="Código Postal" name="campo_6_page_3">
                    
                    <input type="text" class="form-control mt-3" placeholder="País" name="campo_7_page_3">
                </div>
                <h5 class="mt-2">4. Señale la dirección electrónica de la página de internet del responsable</h5>
                <input type="text" class="form-control" id="" name="campo_8_page_3"> 

                <input type="submit">
                
                <nav aria-label="paginacion" class="d-flex justify-content-center mt-3">
                    <ul class="pagination pagination-md">
                    <li class="page-item ">
                            <a class="page-link" >1</a>
                        </li>
                        <li class="page-item  ">
                            <a class="page-link">2</a>
                        </li>
                        <li class="page-item <?= service('request')->uri->getPath() == 'dash/seccion/seccion_tres'? 'active' :''?>" >
                            <a href="<?= base_url(route_to('dash/seccion/seccion_tres'))?>" class="page-link" name="page#3">3</a>
                        </li>
                        <li class="page-item">
                            <a class="page-link" name="page#4">4</a>
                        </li>
                        <li class="page-item">
                            <a class="page-link" name="page#5">5</a>
                        </li>
                        <li class="page-item">
                            <a class="page-link" name="page#6">6</a>
                        </li>
                        <li class="page-item">
                            <a class="page-link" name="page#7">7</a>
                        </li>
                        <li class="page-item">
                            <a class="page-link" name="page#8">8</a>
                        </li>
                        <li class="page-item">
                            <a class="page-link" name="page#9">9</a>
                        </li>
                        <li class="page-item">
                            <a class="page-link" name="page#10">10</a>
                        </li>
                        <li class="page-item">
                            <a class="page-link" name="page#11">11</a>
                        </li>
                        <li class="page-item">
                            <a class="page-link" name="page#12">12</a>
                        </li>
                        <li class="page-item">
                            <a class="page-link" name="page#13">13</a>
                        </li>
                    </ul>
                </nav>
                
            </form>

            
            <div class="buttons_seccion1 d-flex justify-content-center mt-3 ">
                <a href="<?= base_url(route_to('dash/seccion/seccion_dos'))?>" class="btn btn-primary btn-sm mr-3 ml-3" name="regresar" id="btnRegresar">Regresar</a>
                <a  class="btn btn-secondary btn-sm mr-3 ml-3"  name="limpiar" id="btnLimpiar">Limpiar</a>
                <a href="<?= base_url(route_to('dash/seccion/seccion_cuatro'))?>" class="btn btn-primary btn-sm mr-3 ml-3"name="siguiente" id="btnSiguiente">Siguiente</a>
            </div>
        </div>
    </div>

<?= $this->endSection();?>
<?php $this->extend("Template/Base");?>