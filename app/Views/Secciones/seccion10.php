<?php $this->extend("Template/Base");?>
<?= $this->section("content");?>
    <div class="contenido_seccion10 d-flex flex-column justify-content-center">
    <?= $this->include('Dashboard/header_user');?>
        <div class="mr-5 ml-5 mb-5 border border-dark p-3">
            <form>
                <h4 class="mt-2">Seccion X. Opciones y medios para limitar el uso y divulgación de los datos personales</h4>
                <h5 class="mt-3">1. ¿El aviso de privacidad que generá a través de esta herramienta lo dará a conocer a través de su página de internet?</h5>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                    <label class="form-check-label" for="exampleRadios2">Si</label>
                    <br/>    
                    <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                    <label class="form-check-label" for="exampleRadios2">No</label>
                </div>

                <h5 class="mt-3">2. ¿Su página de internet utiliza cookies, web beacons u otras técnologias similares para recabar datos personales de los visitantes?</h5>
                <div class="form-check">    
                    <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                    <label class="form-check-label" for="exampleRadios2">Si</label>
                    <br/>    
                    <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                    <label class="form-check-label" for="exampleRadios2">No</label>
                </div>

                <h5 class="mt-3">3. Señale los datos personales que recaba con estas técnologias del rastreo</h5>
                <div class="form-check">
                    <input class="form-check-input " type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Identificadores, nombre de usuario y contraseñas de una sesión</label>
                    <br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Idioma preferido por el usuario</label>
                    <br>
                    
                    <input class="form-check-input " type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Región en la que se encuentra el usuario</label>
                    <br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Tipo de navegador del usuario</label>
                    <br>
                    
                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Tipo de sistema operativo del usuario</label>
                    <br>
                    
                    <input class="form-check-input " type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Fecha y hora de inicio y final de una sesión de un usuario</label>
                    <br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Páginas web visitadas por un usuario</label>
                    <br/>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Búsquedas realizadas por un usario</label>
                    <br>
                    
                    <input class="form-check-input " type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Publicidad revisada por un usuario</label>
                    <br>

                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">Listas y hábitos de consumo en página de compras</label>
                    <br/>
                    
                </div>
                <h5 class="mt-3">Otros datos personales</h5>
                <input type="text"  class="form-control">

                <h5 class="mt-3">4. Indique las finalidades para las cuales utilizará los datos personales que obtiene a través de estas tecnologías</h5>
                <input type="text"  class="form-control">

                <h5 class="mt-3">5. Indique las finalidades para las cuales utilizará los datos personales que obtiene a través de estas tecnologías</h5>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                    <label class="form-check-label" for="exampleRadios2">Si</label>
                    <br/>    
                    <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                    <label class="form-check-label" for="exampleRadios2">No</label>
                </div>
            </form>
            <nav aria-label="paginacion" class="d-flex justify-content-center mt-3">
                <ul class="pagination pagination-md">
                    <li class="page-item ">
                        <a class="page-link" >1</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" >2</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#3">3</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#4">4</a>
                    </li>
                    <li class="page-item ">
                        <a class="page-link" name="page#5">5</a>
                    </li>
                    <li class="page-item" >
                        <a class="page-link" name="page#6">6</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#7">7</a>
                    </li>
                    <li class="page-item ">
                        <a class="page-link" name="page#8">8</a>
                    </li>
                    <li class="page-item ">
                        <a class="page-link" name="page#9">9</a>
                    </li>
                    <li class="page-item <?= service('request')->uri->getPath() == 'dash/seccion/seccion_diez'? 'active' :''?>">
                        <a href="<?= base_url(route_to('dash/seccion/seccion_diez'))?>" class="page-link" name="page#10">10</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#11">11</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#12">12</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#13">13</a>
                    </li>
                </ul>
            </nav>
            <div class="buttons_seccion1 d-flex justify-content-center mt-3 ">
                <a href="<?= base_url(route_to('dash/seccion/seccion_nueve'))?>" class="btn btn-primary btn-sm mr-3 ml-3" name="regresar" id="btnRegresar">Regresar</a>
                <button type="button" class="btn btn-secondary btn-sm mr-3 ml-3"  name="limpiar" id="btnLimpiar">Limpiar</button>
                <a href="<?= base_url(route_to('dash/seccion/seccion_once'))?>" class="btn btn-primary btn-sm mr-3 ml-3"name="siguiente" id="btnSiguiente">Siguiente</a>
            </div>
        </div>
    </div>
<?= $this->endSection();?>
<?php $this->extend("Template/Base");?>