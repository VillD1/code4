<?php $this->extend("Template/Base");?>
<?= $this->section("content");?>
    <div class="contenido_seccion11 d-flex flex-column justify-content-center">
    <?= $this->include('Dashboard/header_user');?>
        <div class="mr-5 ml-5 border border-dark p-3">
            <form>
                <h4 class="mt-2">Seccion XI. Camibos de aviso de privacidad</h4>
                <h5 class="mt-3">1. Describa los medios que tiene implementados para dar a conocer los cambios o actualizaciones del aviso de privacidad</h5>
                <input type="text"  class="form-control">
                <h5 class="mt-3">2. ¿Por qué tipo de medio dará a conocer los cambios al aviso de privacidad?</h5>
                <div class="form-check">    
                    <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                    <label class="form-check-label" for="exampleRadios2">Por un medio de entrega directa al titular, como por ejemplo el envio de correo electrónico o de correspondencia a su domicilio, o a través de una llamada telefónica.</label>
                    <br/>    
                    <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                    <label class="form-check-label" for="exampleRadios2">Por un medio indirecto, como por ejemplo la publicación de un anuncio en su portal de internet o carteles en su sucursal.</label>
                    <br/>    
                    <input class="form-check-input" type="radio" name="" id="exampleRadios2" value="option2">
                    <label class="form-check-label" for="exampleRadios2">Por ambos.</label>
                </div>
                <h5 class="mt-3">3. Describa el procedimiento para dar a conocer los cambios al aviso de privacidad</h5>
                <input type="text"  class="form-control"> 
            </form>

            <nav aria-label="paginacion" class="d-flex justify-content-center mt-3">
                <ul class="pagination pagination-md">
                    <li class="page-item ">
                        <a class="page-link" >1</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" >2</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#3">3</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#4">4</a>
                    </li>
                    <li class="page-item ">
                        <a class="page-link" name="page#5">5</a>
                    </li>
                    <li class="page-item" >
                        <a class="page-link" name="page#6">6</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" name="page#7">7</a>
                    </li>
                    <li class="page-item ">
                        <a class="page-link" name="page#8">8</a>
                    </li>
                    <li class="page-item ">
                        <a class="page-link" name="page#9">9</a>
                    </li>
                    <li class="page-item ">
                        <a  class="page-link" name="page#10">10</a>
                    </li>
                    <li class="page-item <?= service('request')->uri->getPath() == 'dash/seccion/seccion_once'? 'active' :''?>">
                        <a href="<?= base_url(route_to('dash/seccion/seccion_once'))?>" class="page-link" name="page#11">11</a>
                    </li>
                    <li class="page-item">
                        <button class="page-link" name="page#12">12</button>
                    </li>
                    <li class="page-item">
                        <button class="page-link" name="page#13">13</button>
                    </li>
                </ul>
            </nav>
            <div class="buttons_seccion1 d-flex justify-content-center mt-3 ">
                <a href="<?= base_url(route_to('dash/seccion/seccion_diez'))?>" class="btn btn-primary btn-sm mr-3 ml-3" name="regresar" id="btnRegresar">Regresar</a>
                <button type="button" class="btn btn-secondary btn-sm mr-3 ml-3"  name="limpiar" id="btnLimpiar">Limpiar</button>
                <a href="<?= base_url(route_to('dash/seccion/seccion_doce'))?>" class="btn btn-primary btn-sm mr-3 ml-3"name="siguiente" id="btnSiguiente">Siguiente</a>
            </div>
        </div>
    </div>
<?= $this->endSection();?>
<?php $this->extend("Template/Base");?>