<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document Title</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css" integrity="sha512-Fo3rlrZj/k7ujTnHg4CGR2D7kSs0v4LLanw2qksYuRlEzO+tcaEPQogQ0KaoGN26/zrn20ImR1DfuLWnOo7aBA==" crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>/CSS/estilos.css">

    
</head>
<body>
    <div class="container-fluid flex-column" style="min-hight:100vh;">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand font-weight-bold" href="<?=base_url('/')?>" >LOGOTIPO</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <?php if(session()->logged):?>
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <div class="dropdown">
                                    <a class="btn dropdown-usuario" data-toggle="dropdown">
                                        <?=session('User')?>
                                        <i class="fas fa-cog cog-user"></i>
                                    </a>
                                <div class="dropdown-menu border-0 menu-usuario" aria-labelledby="dropdownMenuButton" id="id01">
                                    <a class="dropdown-item menu-usuario-item" href="<?= base_url(route_to('usuario_modificar'))?>">Editar perfil</a>
                                    <a class="dropdown-item menu-usuario-item" href="<?= base_url(route_to('signout'))?>">Cerrar seción</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            <?php else: ?>
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <button type="button" onclick="document.location='#ventana_sesion'"  class="border-0 bg-white"> 
                        Inciar Sesión </button>
                    </li>
                </ul>
                </div>
            <?php endif;?>
        </nav>
        <?= $this->renderSection('content');?>
        

        <?= $this->renderSection('content');?>
        
        <div class="contenedor__footer row">
            <div class="col-6 justify-content-center">
                <h1>LOGOTIPO</h1>
                    <div class="container__footer-icon">
                        <a class="mr-3" href="">Ayuda</a>
                        <a class="mr-3" href="">Privacidad</a>
                        <a class="mr-3" href="">Condiciones</a>
                    </div>
            </div>
            <div class="col-6">
                <p >Instituto Nacional de Transparencia, Acceso a la Información y Protección de Datos Personales Insurgentes Sur No. 3211 Col. Insurgentes Cuicuilco, Alcaldía Coyoacán, C.P. 04530 Tel. 01 800 8354324</p>
            </div>
        </div>
    </div>
    
    

    <!--<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>-->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>

</body>
</html>