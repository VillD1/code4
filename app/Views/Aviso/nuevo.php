<?php $this->extend("Template/Base");?>
<?= $this->section("content");?>
    
    <div class="contenido__crear d-flex flex-column justify-content-center">
        
        <?= $this->include('Dashboard/header_user');?>
        <?php
            require_once ('app\Controllers\Avisos\Crud_Avisos.php');
            $avisoModel = new \App\Controllers\Avisos\Crud_Avisos();
        ?>
        
    
        <div class="sub_titulo_crear mr-5 ml-5 border border-dark p-3">
            <p>Para generar el aviso de privacidad le solicitamos conteste el siguiente cuestionario. Esto puede tomar de una y media a dos horas aproxímadamente. Tome en cuenta que en caso de que no sea posible concluir el aviso en una sola sesión, la información se guarda para su llenado posterior.</p>
            <p>Para responder el cuestionario le recomendamos leer El ABC del Aviso de Privacidad.</p>

            <button type="button" class="btn btn-primary" data-toggle="modal"data-target="#staticBackdrop">Crear Aviso </button>
            <button type="button" class="btn btn-secondary">Cancelar</button>
            <div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="staticBackdropLabel">Estimado usuario:</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Hacemos de su conocimiento que la información que a continuación le será solicitada tiene la finalidad de que usted pueda generar un aviso de privacidad con los elementos informativos que requiere la norma, y que responda a las necesidades de su negocio, empresa u organización, así como a las características específicas del tratamiento que realiza.</p>
                            <p>La información que nos proporcione a través del Generador de Avisos de Privacidad (GAP) será protegida conforme a lo dispuesto por la Ley General de Protección de Datos Personales en Posesión de Sujetos Obligados, y demás normatividad que resulte aplicable.
                            </p>
                            <p>Por otra parte, es importante precisar que la veracidad de la información proporcionada para generar el aviso de privacidad respectivo es responsabilidad del usuario, por lo que la elaboración de un aviso de privacidad que cumpla con la norma dependerá de que lo manifestado en el mismo se apegue a la realidad del tratamiento al que refiere. Asimismo, generar un aviso de privacidad a través de esta herramienta, no exime a los responsables de su obligación de ponerlo a disposición de los titulares de los datos personales, en el momento y por los medios que establecen los Lineamientos del Aviso de Privacidad.
                            </p>
                            <p>En ese sentido, la elaboración del aviso de privacidad a través de esta herramienta no prejuzga sobre lo que, en su caso, pudiera determinar el Pleno del INAI en el ejercicio de las facultades que le han sido conferidas, o lo que sea resuelto en algún procedimiento sustanciado ante este Instituto, ni valida que el responsable lleve a cabo el tratamiento de datos personales de acuerdo con lo declarado en el aviso de privacidad.
                            </p>
                            <p>
                            El Generador de Avisos de Privacidad no equivale a una autorización ni a un visto bueno, sino que se limita a facilitar la elaboración de avisos de privacidad de conformidad con los elementos que indican la Ley Federal de Protección de Datos Personales en Posesión de los Particulares, su Reglamento y los Lineamientos del Aviso de Privacidad. Siendo esto último el objeto principal del sistema, los datos contenidos en el mismo NO serán utilizados por la autoridad con fines de verificación o imposición de sanciones.
                            </p>
                            <p>
                            Le deseamos una grata experiencia en el uso del GAP.
                            </p>
                        </div>
                        <?php 
                            if(isset($_GET['crearAviso'])){
                                $id = $avisoModel->saveAviso();
                                echo "<script> window.location.href = '/dash/seccion/seccion_uno?id=".$id."';</script>";
                            }
                            ?>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <form action="" method="GET">
                                <button type="submit" class="btn btn-primary" name="crearAviso" id="Aceptar_Modal">Aceptar</button>
                            </form>
                            
                        </div>
                        
                    </div>
                </div>
            </div>         
        </div>
    </div>
    <?= $this->endSection();?>
    <?php $this->extend("Template/Base");?>