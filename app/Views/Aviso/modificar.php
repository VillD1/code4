<?php $this->extend("Template/Base");?>
<?= $this->section("content");?>

    <div class="d-flex justify-content-center align-items-center modificar-cuenta">
        
        <form action="<?= base_url('dash/edit')?>" method="POST">
        <h1 class="mb-4 mt-4 text-center">CUENTA DE USUARIO</h1>

            <input type="hidden" value=<?= $datos->idPersona?> name="id">
            <div class="mb-4 mt-5 inputIconEmail">        
                <input type="text"class="form-control input-form " name='razonSocial' placeholder="Razon Social" class="form-control" value=<?=$datos->RazonSocial?>> 
                <img class="img-svg"src="/Recursos/home_house_real_icon.svg" >
                <span class="text-danger"> 
                    <?= isset($validation)? '<div class="ml-3 mr-2 d-inline"> <i class="fas fa-exclamation-circle"></i></div>'. mostrar_error($validation, 'razonSocial'):'';?>
                </span>
            </div>

            <div class="mb-4 mt-5 inputIconEmail">
                <input type="email" class="form-control input-form " name='email' placeholder="Email" aria-describedby="emailHelp" value=<?=$datos->Email?>>
                <img class="img-svg"src="/Recursos/email_letter_mail_icon.svg" >
            </div>

            <div class="mb-4 mt-5 inputIconEmail">
                <input type="text" class="form-control input-form" placeholder="Usuario" name="user" 
                value=<?=$datos->User?>>
                <img class="img-svg" src="/Recursos/user_app_basic_icon.svg">
            </div>

            <div class="mb-4 mt-5 inputIconEmail">
                <input type="password" class="form-control input-form icon-form-image" id="exampleInputPassword1" placeholder="Contraseña" name="contraseña"  >
                <img class="img-svg" src="/Recursos/security_password_protect.svg" >
            </div>

            <div class="mb-5 mt-5 inputIconEmail">
                <input type="password" class="form-control input-form icon-form-image" id="exampleInputPassword1" placeholder="Confirmar Contraseña" name="cContraseña">
                <img class="img-svg" src="/Recursos/security_password_protect.svg" >
            </div>

            <div class="d-flex justify-content-around align-items-center mb-3">
                <a class="text-decoration-none " href="<?=base_url(route_to('crear_avisos'))?>">Cancelar</a>
                <input type="submit" ></input>
            </div>
            
        </form>
    </div>

<?= $this->endSection();?>
<?php $this->extend("Template/Base");?>