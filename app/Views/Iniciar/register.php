<?php $this->extend("Template/Base");?>
<?= $this->section('content');?>

<div class="contenedor__registrar bg-danger row justify-content-center" id="contenedor_registrar">

            <div class="col-12 " style="margin:auto">
                <div class="titulo__registrar text-center">
                    <h1>Crear cuenta de empresa</h1>
                    <h3>Iniciar sesion en su cuenta</h3>
                </div>
                <div class="form__registrar">
                    <div class= "d-flex justify-content-center" >

                        <form action="<?= base_url('auth/save');?>" method="POST" style="width:60vw">
                        <?php if(!empty(session()->getFlashdata('warning'))):?>
                            <div class="alert alert-warning" role="alert">
                                <?=session()->getFlashdata('warning');?> 
                            </div>
                        <?php endif?>
                        <?php if(!empty(session()->getFlashdata('success'))):?>
                            <div class="alert alert-success" role="alert">
                                <?=session()->getFlashdata('success');?>    
                            </div>
                        <?php endif?>
                            <div class="form-group inputIconEmail mb-4 mt-5">
                                <input id="nombre" class="form-control input-form" type="text" name="nombre" value='<?= old('nombre')?>' placeholder="Nombre o Razon Social">
                                <img class="img-svg"src="/Recursos/home_house_real_icon.svg" >
                                <span><?= isset($validation)? mostrar_error($validation, 'nombre'):'';?></span>
                                
                            </div>

                            <div class="form-group inputIconEmail mb-4 mt-5">
                                <input id="email" class="form-control input-form" type="text" name="email" value='<?= old('email')?>'placeholder="E-mail">
                                <img class="img-svg"src="/Recursos/email_letter_mail_icon.svg" >
                                <span><?= isset($validation)? mostrar_error($validation, 'email'):'';?></span>
                                
                            </div>
                                        
                            <div class="form-group inputIconEmail mb-4 mt-5">
                                <input id="usuario" class="form-control input-form" type="text" name="usuario" value='<?=  old('usuario')?>'placeholder="Usuario">
                                <img class="img-svg" src="/Recursos/user_app_basic_icon.svg">
                                <span><?= isset($validation)? mostrar_error($validation, 'usuario'):'';?></span>

                                
                            </div>
                                        
                            <div class="form-group inputIconEmail mb-4 mt-5">
                                <input id="contraseña" class="form-control input-form" type="password" value='<?= old('contraseña');?>'name="contraseña" placeholder="Contraseña">
                                <img class="img-svg" src="/Recursos/security_password_protect.svg" >
                                <span><?= isset($validation)? mostrar_error($validation, 'contraseña'):'';?></span>
                                
                            </div>
                                        
                            <div class="form-group inputIconEmail mb-4 mt-5">
                                <input id="Ccontraseña" class="form-control input-form" type="password" value='<?= old ('Ccontraseña');?>'name="Ccontraseña" placeholder="Confirmar Contraseña">
                                <img class="img-svg" src="/Recursos/security_password_protect.svg" >
                                <span><?= isset($validation)? mostrar_error($validation, 'Ccontraseña'):'';?></span>
                                
                            </div>

                            <div class="form-group form-check">
                                <input type="checkbox" class="form-check-input " id="exampleCheck1">
                                <label class="form-check-label text-light" for="exampleCheck1">Check me out</label>
                            </div>
                            <a class="d-flex justify-content-end text-light"  href="#ventana_sesion">Ya tengo una cuenta</a>

                            <div class="form-group mt-5 d-flex justify-content-center">
                                <button class="btn btn-primary btn block rounded-pill bg-light text-dark" style="width:40%; font-size:25px" type="submit">Guardar</button>
                            </div>
                            <br>
                            <div class="text-center">
                                <a class="mr-3 ml-3" href="">Ayuda</a>
                                <a class="mr-3 ml-3" href="">Privacidad</a>
                                <a class="mr-3 ml-3" href="">Condiciones</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?= $this->endSection();?>
        <?php $this->extend("Template/Base");?>

