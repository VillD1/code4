<?php $this->extend("Template/Base");?>
<?= $this->section('content');?>

        <div class="row contenedor__parrafos">
            <div class="col-6 contenedor__item-1 d-flex justify-content-center align-items-center p-4 bg-secondary w-100">
                <p>"El sistema Generador de Avisos de Privacidad, tanto para sector público como privado, es una solución informática que ha sido desarrollada para facilitar la creación de avisos de privacidad que los responsables del tratamiento de datos personales, del sector público y privado, tienen la obligación de poner a disposición de los titulares de los mismos, conforme a la normativa que les resulta aplicable."</p>
            </div>
            <div class="col-6 contenedor__item-2 d-flex flex-column justify-content-center align-items-center p-4 bg-danger">
                <H1>GENERADOR</H1>
                <h2>DE AVISOS DE PRIVACIDAD</h2>
                <h1>SECTOR PRIVADOR</h1>
            </div>
        </div>

        <div id="ventana_sesion" class="ventana__2">
            <div class="titulo__iniciar d-flex flex-column align-items-center">
                <h3 class="titulo__iniciar_item-1  mt-3">Iniciar sesion en su cuenta</h3>
                <div class="rectangulo"></div>
                <h1 class="titulo__iniciar_item-2 mt-3">BIENVENIDO DE NUEVO</h1>
            </div>
            <div class="d-flex align-items-center justify-content-center"style="height:calc(100vh - 150px);">
                <div class="contenedor__iniciar row">
                        <div class="col-6 d-flex justify-content-center align-items-center">
                <img class="" src="<?= base_url()?>\Recursos\Login.png" alt="" width="70%" >
                        </div>
                        <div class="col-6 form__iniciar d-flex align-items-center p-0">
                            
                            <form action="<?= base_url('auth/validate')?>" method="POST" class="form_login d-flex flex-column flex-grow-1">

                            <?php if(!empty(session()->getFlashdata('fail'))):?>
                            <div class="alert alert-warning" role="alert">
                                <?=session()->getFlashdata('fail');?> 
                            </div>
                            <?php endif?>
                            <?php if(!empty(session()->getFlashdata('success'))):?>
                            <div class="alert alert-success" role="alert">
                                <?=session()->getFlashdata('success');?>    
                            </div>
                            <?php endif?>
                                <div class="form-group mb-4 mt-4 inputIconEmail">
                                    <input class="form-control input-form " type="text" name="user" placeholder="Insertar Email">
                                    <img class="img-svg"src="/Recursos/email_letter_mail_icon.svg" >
                                    
                                    <span class="text-danger"> 
                                        <?= isset($validation)? '<div class="ml-3 mr-2 d-inline"> <i class="fas fa-exclamation-circle"></i></div>'. mostrar_error($validation, 'user'):'';?>
                                    </span>
                                </div>
                                <div class="form-group mb-4 mt-4 inputIconEmail">
                                    <input class="form-control input-form" type="password" name="password" placeholder="Insertar Contraseña">
                                    <img class="img-svg" src="/Recursos/security_password_protect.svg" >
                                    <span class="text-danger"><?= isset($validation)? '<div class="ml-3 mr-2 d-inline"> <i class="fas fa-exclamation-circle"></i></div>'. mostrar_error($validation, 'password'):'';?></span>
                                </div>
                                <div class="">
                                    <a class ="link"href="#">¿Contraseña olvidada?</a>
                                    <p>¿No tienes una cuenta?<a href="#contenedor_registrar">Registrarse</a></p>
                                </div>
                                <br>
                                <div class="d-flex justify-content-center form-group ">
                                    <button class="btn_iniciar btn btn-primary btn block" type="submit">Ingresar</button>
                                </div>
                                <br>
                                <div class="enclaces d-flex justify-content-center">
                                    <a class="enlace_iniciar mr-2 ml-2" href="">Ayuda</a>
                                    <a class="enlace_iniciar mr-2 ml-2" href="">Privacidad</a>
                                    <a class="enlace_iniciar mr-2 ml-2" href="">Condiciones</a>
                                </div>
                            </form>
                        </div>
                </div>
            </div>
        </div>
<?= $this->endSection();?>
<?php $this->extend("Template/Base");?>

        

        

    

