
<div class=" titulo d-flex bg-secondary mr-5 ml-5">

    <div class="col-4 text-center p-2 text-white rounded-0 btn btn-primary <?= service('request')->uri->getPath() == 'dash/vista_crear'? 'active' :''?>" >
        <a class="d-block text-decoration-none text-white"  href="<?= base_url(route_to('crear_avisos'))?>">Nuevo Aviso</a>
    </div> 

            
    <div class="col-4 text-center p-2 text-white rounded-0 btn btn-primary <?= service('request')->uri->getPath() == 'dash/vista_detalle'? 'active' :''?> " >
        <a class="d-block text-decoration-none text-white" href="<?= base_url(route_to('detalle_avisos'))?>" >Mis Avisos</a>
    </div>

    <div class="col-4 text-center p-2 text-white rounded-0 btn btn-primary <?= service('request')->uri->getPath() == 'dash/usuario_modficiar'? 'active' :''?>" > 
        <a class="d-block text-decoration-none text-white" href="<?= base_url(route_to('usuario_modificar'))?>" >Usuario</a>
    </div>
</div>
