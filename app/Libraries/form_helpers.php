<?php
    function mostrar_error($validation, $campo){
        if($validation->hasError($campo)){
            return $validation->getError($campo);
        }else{
            return false;
        }
    }
?>