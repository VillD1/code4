<?php 
namespace App\Controllers\Auth;

use CodeIgniter\Controller;
use App\Models\Usuario;

class Register extends Controller{
    
    public function __construct(){
        helper(['url', 'form_helpers']);
    }
    public function index(){
        return view('Iniciar/register');
    }
    public function guardar(){
        $validation = $this->validate([
            'nombre'=>[
                'rules'=>'required',
                'errors'=>[
                    'required'=>'Campo requerido'
                ]
            ],
            'email'=>[
                'rules'=>'required',
                'errors'=>[
                    'required'=>'Campo requerido'
                ]
            ],
            'usuario'=>[
                'rules'=>'required',
                'errors'=>[
                    'required'=>'Campo requerido',
                ]
            ],
            'contraseña'=>[
                'rules'=>'required',
                'errors'=>[
                    'required'=>'Campo requerido'
                ]
            ],
            'Ccontraseña'=>[
                'rules'=>'required',
                'errors'=>[
                    'required'=>'Campo requerido'
                ]
            ],
        ]);
        if(!$validation){
            return view('Iniciar/register',['validation'=>$this->validator]);
            
        }else{
            $user= $this->request->getPost('usuario'); 
            $db = \Config\Database::connect();
            $userModel = new Usuario();
            $query = $db->query('SELECT * FROM persona WHERE User = "'.$user.'"');
            $resultado = $query->getResult();
            if(!$resultado){
                $userModel->insert(
                    [
                        'RazonSocial' => $this->request->getPost('nombre'),
                        'User' => $this->request->getPost('usuario'), 
                        'Pass' => $this->request->getPost('contraseña'),
                        'Email' =>$this->request->getPost('email'),
                        'Estado' => 1,
                        'Fecha'=> date('Y-m-d'),
                    ]);
                    return redirect()->route('login')->with('success', 'Exito');
            }else{
                return redirect()->route('register')->with('warning', 'Usuarios ya registrado');
            }
        }
    }
}