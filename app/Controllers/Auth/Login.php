<?php 
namespace App\Controllers\Auth;

use CodeIgniter\Models;
use CodeIgniter\Controller;


class Login extends Controller{
    
    public function __construct(){
        helper(['url', 'form_helpers']);
    }
    public function index(){
        if(!session()->logged){
            return view('Iniciar/login');
        }
        return redirect()->route('crear_avisos');
    }
    
    public function validarUsuario(){
        $validation = $this->validate([
            'user'=>[
                'rules'=>'required',
                'errors'=>[
                    'required'=>'Campos requerido'
                ]
            ],
            'password'=>[
                'rules'=>'required',
                'errors'=>[
                    'required'=>'Campos requerido'
                ]
            ]
        ]);
        if(!$validation){
            return view('Iniciar/login',['validation'=>$this->validator]);
        }
            
            $email=$this->request->getPost('user');
            $pass=$this->request->getPost('password');
            
            $db = \Config\Database::connect();
            $query = $db->query('SELECT * FROM persona WHERE Email = "'.$email.'" AND Pass = "'.$pass.'" ');
            $resultado = $query->getResult();
            $userModel = new \App\Models\Usuario();
            $userInfo = $userModel->getUserBy('Email',$email);

            //if(!$user = $model->getUserBy('Email',$email)){
                //return redirect()->back()->with('danger', 'Este usuario no se encuentra registrado');
            //}
            if(!$resultado){
                return redirect()->back()->with('fail', 'Credenciales incorrectas');
                //->with('danger', 'Este usuario no se encuentra registrado');
            }

                session()->set([
                    'idPersona'=>$userInfo->idPersona,
                    'User'=>$userInfo->User,
                    'logged'=>true
                ]);
                return redirect()->route('crear_avisos')->with('msg',[
                    'type'=>'success',
                    'body'=>'Bienvenido'.$userInfo->User
                ]);
            /*return redirect()->route('crear_avisos');
            if(!$resultado){
                session()->setFlashdata('fail','Sucedio un error');
                return redirect()->route('login')->withInput();*/
            
    }
    public function signout(){
        session()->destroy();
        return redirect()->route('login');
    }
}