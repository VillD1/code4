<?php
namespace App\Controllers\Avisos;

use CodeIgniter\Controller;
use App\Models\Aviso;

class Crud_Avisos extends Controller{

    public function nuevo(){
        return view('Aviso/nuevo');
    }

    public function saveAviso(){
        $avisosModel = model('Aviso');                                                                              
        $folioAviso = "7020";
        $usuario = session()->get('idPersona');
        
        $avisosData = $avisosModel->insert([
            "folioAviso"=>$folioAviso,
            "FechaCreacion"=>date('Y-m-d'),
            "FechaModificacion"=>date('Y-m-d'),
            "fk_idPersona"=> $usuario,
            "Estado"=>1,
            ]);
            
            $idAviso = $avisosModel->insertID();
            
            return $idAviso;
    }
    public function insertAviso(){
        $empresaModel = model ('Empresa');

        $empresaModel->insert([
            'fk_idRamas'=>$this->request->getPost('ramas'),
            'fk_idSector'=>$this->request->getPost('sector'),
        ]);
        return 1;
    }
    public function updateAviso($tipoEmpresa, $idAviso){
        $avisosModel = model('Aviso');

        $avisoData = $avisosModel->set('fk_idTipoEmpresa', $tipoEmpresa);//$this->request->getPost('tipoEmpresa'));
        $avisoData = $avisosModel->where('idAviso', $idAviso);//$idAviso-----------32420 representa el id del aviso.
        $avisoData = $avisosModel->update();
        return 1;
    }

        /*
            $db = \Config\Database::connect();
            $query = $db->query('UPDATE aviso SET fk_idTipoEmpresa = 1 WHERE aviso idAviso = 32420');
            $resultado = $query->getResult();
        */
}
?>