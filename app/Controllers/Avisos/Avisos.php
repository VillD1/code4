<?php 
namespace App\Controllers\Avisos;

use CodeIgniter\Controller;
use App\Models\Aviso;
use App\Models\Ramas;
use App\Models\TipoAviso;
use App\Models\Seccion3;

class Avisos extends Controller{
    
    public function __construct(){
        helper(['url', 'form_helpers']);
    }
    public function detalle_aviso(){
        return view('Aviso/detalle');
    }
    public function modificar_user(){
        $usuarioModel = model('Usuario');
        $usuarioQuery = $usuarioModel->where('idPersona',session()->get('idPersona'))->first();
        $usuarioData['datos'] = $usuarioQuery;
        
        return view('Aviso/modificar',$usuarioData);
    }
    public function actualizar_user(){

        $usuarioModel = model('Usuario');
        $usuarioQuery = $usuarioModel->update(
            $this->request->getPost('idPersona'),
            ['RazonSocial'=>$this->request->getPost('razonSocial'),
            'Email'=>$this->request->getPost('email'),
            'User'=>$this->request->getPost('user'),
            'Pass'=>$this->request->getPost('contraseña'),
            ]);
        return redirect()->to(base_url('dash/vista_modificar'));
    }

    public function seccion_1(){

        $sectorModel = model('Sector');
        $tipoEmpresaModel = model('TipoEmpresa');
        

        $dataSectores['sectores'] = $sectorModel->findAll();
        $dataSectores['tipoEmpresa'] = $tipoEmpresaModel->findAll();
        

        return view('Secciones/seccion1',$dataSectores);
    }

    public function action(){

        if($this->request->getVar('action'))
        {
            $action = $this->request->getVar('action');
            if($action == 'get_ramas')
            {
                $ramasModel = model('Ramas');
                $ramasData = $ramasModel->where('fk_idSector',$this->request->getVar('idSector'))->findAll();
                echo json_encode($ramasData);
            }
        }
    }
    
    public function seccion_2(){
        $tipoAvisoModel = new TipoAviso();
        $dataTipoAviso['tipoAviso'] = $tipoAvisoModel->findAll();

        return view('Secciones/seccion2', $dataTipoAviso);
    }
    public function seccion_3(){

        return view('Secciones/seccion3');
    }
    public function seccion_3_save(){
        $Seccion3Model = new Seccion3();
        $campo1 = $this->request->getPost('campo_1_page_3');
        $campo2 = $this->request->getPost('campo_2_page_3');
        $campo3 = $this->request->getPost('campo_3_page_3');
        $campo4 = $this->request->getPost('campo_4_page_3');
        $campo5 = $this->request->getPost('campo_5_page_3');
        $campo6 = $this->request->getPost('campo_6_page_3');
        $campo7 = $this->request->getPost('campo_7_page_3');
        $campo8 = $this->request->getPost('campo_8_page_3');

        $Seccion3Model->insert([
            'RazonSocial'=> $campo1, 
            'NombreComercial'=> $campo2,
            'Calle' => $campo3,
            'Ciudad' => $campo4,
            'Municipio' => $campo5,
            'cPostal' => $campo6,
            'Pais' => $campo7,
            'PaginaWeb' => $campo8,
            'fk_idAviso'=>1,
        ]);
        return redirect()->route('dash/seccion/seccion_tres');
    }

    public function seccion_4(){
        return view('Secciones/seccion4');
    }
    public function seccion_5(){
        return view('Secciones/seccion5');
    }
    public function seccion_6(){
        return view('Secciones/seccion6');
    }
    public function seccion_7(){
        return view('Secciones/seccion7');
    }
    public function seccion_8(){
        return view('Secciones/seccion8');
    }
    public function seccion_9(){
        return view('Secciones/seccion9');
    }
    public function seccion_10(){
        return view('Secciones/seccion10');
    }
    public function seccion_11(){
        return view('Secciones/seccion11');
    }
    public function seccion_12(){
        return view('Secciones/seccion12');
    }
    public function seccion_13(){
        return view('Secciones/seccion13');
    }
    
}